/* c representation of fortran data types*/
#define INTEGER          int
#define DOUBLE_PRECISION double
/* c representation of fortran routines */
#define SECOND           second_
#define GMAINV           gmainv_
#define FMAIN            fmain_
/* c representation of fortran common block name */
#define CORCTL           _corctl_
/* include files */
#include <time.h>
#include <sys/resource.h>

/* DOUBLE PRECISION FUNCTION SECOND() -- return cpu time in seconds */
DOUBLE_PRECISION SECOND()
{ struct rusage ruse; 
  getrusage(RUSAGE_SELF,&ruse);
  return ruse.ru_utime.tv_sec + 0.000001 * ruse.ru_utime.tv_usec;
}


/* SUBROUTINE GMAINV(Q,IBASE,N)
   COMMON /CORCTL/ INTREL,ICORCT(8)
   set INTREL=number of integers per real
   allocate n double precision words; returns ibase such that q(ibase) 
   is the first word allocated 
   SUBROUTINE FMAIN(Q,N)
   release allocated area at q */

static DOUBLE_PRECISION *pointer;

GMAINV(q,ibase,n) DOUBLE_PRECISION *q; INTEGER *ibase, *n;
{ extern struct { INTEGER intrel; INTEGER icorct[8]} CORCTL ;
  CORCTL.intrel = sizeof(DOUBLE_PRECISION)/sizeof(INTEGER);
  pointer=(DOUBLE_PRECISION *) calloc(*n+2,sizeof(DOUBLE_PRECISION));
  *ibase=pointer-q+1; 
}

FMAIN(q,n) DOUBLE_PRECISION *q; INTEGER *n; {free(pointer);}
