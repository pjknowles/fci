         MACRO
&NAME    ROUTINE
         GBLA    &ROUTINC
         GBLC    &ROUTINN
         AIF   ('&ROUTINC' NE '0').NODEF
R0       EQU   0
R1       EQU   1
R2       EQU   2
R3       EQU   3
R4       EQU   4
R5       EQU   5
R6       EQU   6
R7       EQU   7
R8       EQU   8
R9       EQU   9
R10      EQU   10
R11      EQU   11
R12      EQU   12
R13      EQU   13
R14      EQU   14
R15      EQU   15
.NODEF   ANOP
&ROUTINC SETA &ROUTINC+1
         LTORG
&NAME    CSECT
&ROUTINN SETC '&NAME'
         B     88(0,R15)
         DC    X'09',CL9'&NAME'
         DS    18F
         STM   R14,R12,12(R13)
         LR    R12,R13
         LA    R13,16(0,R15)
         ST    R12,4(0,R13)
         ST    R13,8(0,R12)
         USING &NAME.+16,R13
         MEND
         MACRO
&NAME    EXIT  &ARG
         AIF   ('&NAME' EQ '').NONAME
&NAME    EQU   *
.NONAME  AIF   ('&ARG' EQ '').R15DONE
         AIF   ('ARG'(1,1) EQ '(').R15REG
         LA    R15,&ARG
         AGO   .R15DONE
.R15REG  ANOP
.R15DONE ANOP
         L     R13,4(R13)
         L     R14,12(R13)
         MVI   12(R13),255
         LM    R2,R12,28(R13)
         BR    R14
         MEND
         MACRO
&NAME    ENTRYPT
         GBLC  &ROUTINN
         AIF   ('&ROUTINN' NE '&SYSECT').ERROR1
         ENTRY &NAME
         CNOP  0,8
&NAME    B     14(0,R15)
         DC    X'09',CL9'&NAME'
         STM   R14,R12,12(R13)
         LR    R12,R13
         LA    R11,&NAME-&SYSECT-16
         LR    R13,R15
         SR    R13,R11
         ST    R12,4(0,R13)
         ST    R13,8(0,R12)
         USING &SYSECT.+16,R13
         MEXIT
.ERROR1  MNOTE 12,'ENTRYPT must be used in CSECT started with ROUTINE'
         MEND
*
SECOND   ROUTINE
         CLI   SECONDFL,C'N'
         BE    SECOND1
         TTIMER
         L     R1,SECONDT
         SR    R1,R0
         ST    R1,SECONDB
         LD    0,SECONDF
         DD    0,=D'384.0E2'
         EXIT
SECOND1  MVI   SECONDFL,C'Y'
         STIMER TASK,TUINTVL=SECONDT
         SDR   0,0
         EXIT
SECONDF  DS    0D
         DC    X'4E000000'         unnormalised zero
SECONDB  DS    F
SECONDT  DC    X'7FFFFFFF'
SECONDFL DC    C'N'
         LTORG
****************************
CORCTL   COM
INTREL   DS    F
         DS    8F
GMAINV   ROUTINE
*
* CALL GMAINV (Q,IAD,LEN)
* do V-type GETMAIN; length in 8-byte words returned in LEN;
* IAD returned such that first address is Q(IAD), Q r*8;
* On entry, LEN should hold the maximum no. of words wanted.
*
         L     R12,=A(CORCTL)
         USING CORCTL,R12
         LA    2,2
         ST    2,INTREL
         LM    R2,R4,0(R1)
         L     R1,0(R4)
         SLL   R1,3
         ST    R1,GMAINV3+4
         GETMAIN VU,LA=GMAINV3,A=GMAINV4
         L     R1,GMAINV4+4
         SRL   R1,3
         ST    R1,0(R4)              length in 8 byte words
         L     R1,GMAINV4
         SR    R1,2
         SRL   R1,3
         LA    R1,1(R1)
         ST    R1,0(R3)              position in real*8 array Q
         EXIT  0
GMAINV3  DC    F'8'
         DS    F
GMAINV4  DC    F'0,0'
FMAIN    ENTRYPT
*
* CALL FMAIN  (Q,LEN)
* do FREEMAIN; 1st address at Q(1) (Q is r*8); length=LEN*8 bytes
*
         L     R2,0(R1)
         L     R3,4(R1)
         L     R3,0(R3)
         LTR   R3,R3
         BH    FMAINOK
         BL    FMAINERR
         EXIT  0
FMAINOK  SLL   R3,3
         FREEMAIN R,LV=(R3),A=(R2)
         EXIT  0
FMAINERR WTO   'FMAIN called with length < 0',ROUTCDE=11
         ABEND X'30A000'
         LTORG
         END
