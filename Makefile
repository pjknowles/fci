FC=gfortran
FFLAGS=-O3
OBJECTS=fci.o standard.o
EXE=fci

$(EXE):	$(OBJECTS)
	$(FC) $(FFLAGS) -o $@ $(OBJECTS)

.PHONY:	clean
clean:
	rm -f $(OBJECTS) $(EXE)
